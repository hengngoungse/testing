// console.log(document);
//   #" declear by id 
$(document).ready( function() {
    $("#demo2,#demo3").on('click', function(){
      $(this).hide();
    });
  }); 
//  "." declear by class "
$(document).ready(function(){
    $(".button1").click(function(){
        $(".classHide,#idHide1").hide();
    });
});
//
$(document).ready(function(){
    $(".button2").click(function(){
        $("#idHide2").hide();
    });
});
// Selects all <p> elements with class="intro"
$(document).ready(function(){
  $(".button3").click(function(){
    $("p.intro").hide();
  });
});
// 
$(document).ready(function(){
  $("#p1").mouseenter(function(){
    $(".button4").click(function(){
        alert("hi ");
    })
  });
});
// The hover() method takes two functions and is a combination of the mouseenter() and mouseleave() methods.
$(document).ready(function(){
  $("#p3").hover(function(){
    alert("You are enter this sentance. from JQuery BAKA!");
  },
  (function(){
    alert("You are leave this sentance. JQuery BAKA!")
  })
  );
});
// $(document).ready(function(){} can also be deaclar one time
$(document).ready(function(){
  $(".input1").focus(function(){
    $(this).css("background-color","yellow");
  });
  $(".input1").blur(function(){
    $(this).css("background-color","green");
  });
});
// The following example demonstrates how to get the value of an input field with the jQuery val() method:
$(document).ready(function(){
  $(".button5").click(function(){
    alert ("value of name "+$("#test").val());
    console.log(value);
  });
});
// The jQuery remove() method removes the selected element(s) and its child elements.
$(document).ready(function(){
  $(".button6").click(function(){
    $(".demo4").remove();
  });
});
$(document).ready(function(){
  $(".button7").click(function(){
    $("#div1").remove();
  });
});
// The jQuery empty() method removes the child elements of the selected element(s).
$(document).ready(function(){
  $(".p7").click(function(){
    $(".div7").empty();
  });
});
// toggleClass() method. This method toggles between adding/removing classes from the selected elements:
$(document).ready(function(){
  $(".button6").click(function(){
    $("h1, h2, p").toggleClass("green");
  });
});
//The $.get() method requests data from the server with an HTTP GET request.
$(document).ready(function(){
  $(".button8").click(function(){
    $.get("https://api.github.com/users/gaearon",function(data,status){
      alert("Data: " + data +"\nStatus: " + status);
      });
    });
  });
// The $.post() method requests data from the server using an HTTP POST request.
  $(".button9").click(function(){
    $.post("../asp/demo_test_post.asp",
    {
      name: "Donald Duck",
      city: "Duckburg"
    },
    function(data, status){
      alert("Data: " + data + "\nStatus: " + status);
    });
  });

// JQuery can change css html
// $(document.body).css("background-color", "white");
$(function() {
  $( "#danger" ).text( "The DOM is now loaded and can be manipulated." );
  });
// JQuery change css by element
$(document).ready(function(){
  $(".p8").on({
    mouseenter: function(){
      $(this).css("background-color", "lightblue");
      $(this).css("color", "blue");
    },
    mouseleave: function(){
      $(this).css("background-color", "")
      $(this).css("color", "black")
    },
    click: function(){
      $(this).css("background-color", "yellow");
    }
  });
});
// JQuery change css 
$(document).ready(function(){
  $("h3").on({
    mouseenter: function(){
      $(this).css("color", "blue");
    },
    mouseleave: function(){
      $(this).css("color", "red")
    },
    click: function(){
      $(this).css("color", "yellow");
    }
  });
});
// hide element with all href
$(document).ready(function(){
  $("[href]").css("color","blue");
});


// 
    var x = $("#txt1")
    var y = $("#txt2")
    var total = x() + y(); 
$(document).ready(function(){
  $(".btnCalSum").click(function(){
    alert.total("H");
    console.log(value);
  });
});


